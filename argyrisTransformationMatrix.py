from sympy import *
init_printing()


def restoreArrayNotation(code, dic):
  for name, shape in dic.items():
    for i in range(shape[0]):
      for j in range(shape[1]):
        code = code.replace(name+"["+str(i*shape[1]+j)+"]",
                            name+"["+str(i)+"fixed]["+str(j)+"]")
  code = code.replace("fixed", "")
  return code


def writeBCRS(mat, path="argyrisTransformationMethods.txt"):
  with open(path, 'w') as f:
    for row in range(mat.shape[0]):
      print(mat.row(row).RL)
      f.write("mat_.setrowsize("+str(row)+","+str(len(mat.row(row).RL))+");\n")
    f.write("mat_.endrowsizes();\n")
    for row in range(mat.shape[0]):
      for element in mat.row(row).RL:
        f.write("mat_.addindex("+str(row)+","+str(element[1])+");\n")
    f.write("mat_.endindices();\n\n")

    replacementDict = {"theta_0": (3, 3), "theta_1": (3, 3), "theta_2": (3, 3), "J_0": (2, 2),"J_1": (2, 2), "J_2": (2, 2),"thetaDir_0": (3, 3), "thetaDir_1": (3, 3), "thetaDir_2": (3, 3), "dir_0": (2, 2),"dir_1": (2, 2), "dir_2": (2, 2),  "b_1": (
        2, 2), "b_2": (2, 2), "b_0": (2, 2), 't': (3, 2), 'tau': (3, 3)}
    for element in mat.RL:
      code = ccode(element[2], "mat_["+str(element[0]) +
                   "]["+str(element[1])+"]", standard="c99")
      codeRestored = restoreArrayNotation(code, replacementDict)
      f.write(codeRestored+"\n")


E = SparseMatrix(21, 24, {(0, 0): eye(19), (19, 20): 1, (20, 22): 1})
pprint(E)
J_0 = Matrix(MatrixSymbol('J^0', 2, 2))
J_1 = Matrix(MatrixSymbol('J^1', 2, 2))
J_2 = Matrix(MatrixSymbol('J^2', 2, 2))
Dir_0 = Matrix(MatrixSymbol('dir^0',2,2))
Dir_1 = Matrix(MatrixSymbol('dir^1',2,2))
Dir_2 = Matrix(MatrixSymbol('dir^2',2,2))
ThetaDir_0 = Matrix(MatrixSymbol('thetaDir^0',3,3))
ThetaDir_1 = Matrix(MatrixSymbol('thetaDir^1',3,3))
ThetaDir_2 = Matrix(MatrixSymbol('thetaDir^2',3,3))


Theta_0 = Matrix(MatrixSymbol('theta^0', 3, 3))
Theta_1 = Matrix(MatrixSymbol('theta^1', 3, 3))
Theta_2 = Matrix(MatrixSymbol('theta^2', 3, 3))
B1 = Matrix(MatrixSymbol('b^0', 2, 2))
B2 = Matrix(MatrixSymbol('b^1', 2, 2))
B3 = Matrix(MatrixSymbol('b^2', 2, 2))

Dir = SparseMatrix(21, 21, {(0, 0): 1, (1, 1): Dir_0.T, (3, 3): ThetaDir_0, (6, 6): 1, (7, 7): Dir_1.T, (
    9, 9): ThetaDir_1, (12, 12): 1, (13, 13): Dir_2.T, (15, 15): ThetaDir_2, (18, 18): eye(3)})


VC = SparseMatrix(24, 24, {(0, 0): 1, (1, 1): J_0.T, (3, 3): Theta_0, (6, 6): 1, (7, 7): J_1.T, (
    9, 9): Theta_1, (12, 12): 1, (13, 13): J_2.T, (15, 15): Theta_2, (18, 18): B1, (20, 20): B2, (22, 22): B3})
pprint(VC)

l = Matrix(MatrixSymbol('l', 3, 1))

t = Matrix(MatrixSymbol('t', 3, 2))

tau = Matrix(MatrixSymbol('tau', 3, 3))
orientation = Matrix(MatrixSymbol('edgeOrientation', 3, 1))
D = SparseMatrix(24, 21, {(0, 0): eye(19), (20, 19): 1, (22, 20): 1})
shift = [[0, 6], [0, 12], [6, 12]]
for i in range(3):
  D[19+2*i, shift[i][0]] = -15/(8*l[i]) 
  D[19+2*i, shift[i][0]+1] = -7/16 * t[i, :] 
  D[19+2*i, shift[i][0]+3] = -l[i]/32 * tau[i, :] 
  D[19+2*i, shift[i][1]] = 15/(8*l[i]) 
  D[19+2*i, shift[i][1]+1] = -7/16 * t[i, :] 
  D[19+2*i, shift[i][1]+3] = l[i]/32 * tau[i, :]
pprint(D)
pprint(Dir)
# V = E*VC*D
OrientationMatrix = SparseMatrix(21,21,{(0,0):eye(18),(18,18):orientation[0],(19,19):orientation[1],(20,20):orientation[2]})
pprint(OrientationMatrix)
V = OrientationMatrix*Dir*D.T*VC*E.T
pprint(V)
print(V.nnz())
# pprint(V.RL)
# pprint(V[0, :].RL)

print("==============================================================")
print("start writing file")
writeBCRS(V)
#with open("argyrisMatrix.tex", 'w') as f:
 # f.write(latex(D.T*VC*E.T))
