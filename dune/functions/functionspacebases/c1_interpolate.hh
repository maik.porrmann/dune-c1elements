// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUNCTIONS_FUNCTIONSPACEBASES_C1_INTERPOLATE_HH
#define DUNE_FUNCTIONS_FUNCTIONSPACEBASES_C1_INTERPOLATE_HH

#include <memory>
#include <vector>

#include <dune/common/bitsetvector.hh>
#include <dune/common/exceptions.hh>

#include <dune/typetree/childextraction.hh>

#include <dune/functions/common/functionconcepts.hh>
#include <dune/functions/common/signature.hh>
#include <dune/functions/gridfunctions/gridviewfunction.hh>

#include <dune/functions/backends/concepts.hh>
#include <dune/functions/backends/istlvectorbackend.hh>
#include <dune/functions/functionspacebases/flatvectorview.hh>
#include <dune/functions/functionspacebases/hierarchicnodetorangemap.hh>
#include <dune/functions/functionspacebases/sizeinfo.hh>
#include <dune/typetree/traversal.hh>
#include <dune/typetree/visitor.hh>

namespace Dune
{
  namespace Functions
  {
    namespace c1elements
    {

      namespace Imp
      {

        struct AllTrueBitSetVector
        {
          struct AllTrueBitSet
          {
            bool test(int i) const { return true; }
          } allTrue_;

          operator bool() const { return true; }

          template <class I>
          const AllTrueBitSetVector &operator[](const I &) const
          {
            return *this;
          }

          template <class SP>
          void resize(const SP &) const
          {
          }
        };

        template <class B, class T, class NTRE, class HV, class LF, class HBV>
        class LocalInterpolateVisitor:
            public TypeTree::TreeVisitor,
            public TypeTree::DynamicTraversal
        {

        public:
          using Basis = B;
          using LocalView = typename B::LocalView;
          using MultiIndex = typename LocalView::MultiIndex;

          using LocalFunction = LF;

          using Tree = T;

          using VectorBackend = HV;
          using BitVectorBackend = HBV;

          using NodeToRangeEntry = NTRE;

          using GridView = typename Basis::GridView;
          using Element = typename GridView::template Codim<0>::Entity;

          using LocalDomain = typename Element::Geometry::LocalCoordinate;

          using GlobalDomain = typename Element::Geometry::GlobalCoordinate;

          static constexpr int dim = Element::Geometry::coorddimension;
          using This = LocalInterpolateVisitor<B, T, NTRE, HV, LF, HBV>;
          using LocalFunctionDerivativeRange =
              typename SignatureTraits<typename This::LocalFunction::DerivativeSignature>::Range;
          using LocalFunctionHessianRange = typename SignatureTraits<
              typename This::LocalFunction::LocalDerivative::DerivativeSignature>::Range;
          template <typename Node, typename TreePath>
          class DerivativeWithNTRE;
          // TODO improve these helperclasses!
          template <typename Node, typename TreePath>
          class CallableWithNTRE
          {
          public:
            using FiniteElement = typename Node::FiniteElement;
            using FiniteElementRange =
                typename FiniteElement::Traits::LocalBasisType::Traits::RangeType;
            using FiniteElementRangeField =
                typename FiniteElement::Traits::LocalBasisType::Traits::RangeFieldType;

            using FiniteElementJacobianType =
                typename FiniteElement::Traits::LocalBasisType::Traits::JacobianType;

            Node &node_;
            TreePath &treePath_;
            std::size_t &j_ = 0;
            LocalInterpolateVisitor *visitor_ptr_;

          public:
            CallableWithNTRE(Node &node, TreePath &treePath, std::size_t &j,
                             LocalInterpolateVisitor *visitor_ptr)
                : node_(node), treePath_(treePath), j_(j), visitor_ptr_(visitor_ptr)
            {
            }

            FiniteElementRange operator()(const LocalDomain &x) const
            {
              const auto &y = visitor_ptr_->localF_(x);
              return FiniteElementRange(
                  flatVectorView(visitor_ptr_->nodeToRangeEntry_(node_, treePath_, y))[j_]);
            }

            friend auto derivative(CallableWithNTRE const &t) { return DerivativeWithNTRE(t); }
          };

          template <typename Node, typename TreePath>
          class HessianWithNTRE;

          template <typename Node, typename TreePath>
          class DerivativeWithNTRE
          {
          public:
            using FiniteElement = typename Node::FiniteElement;
            using FiniteElementRange =
                typename FiniteElement::Traits::LocalBasisType::Traits::RangeType;
            using FiniteElementRangeField =
                typename FiniteElement::Traits::LocalBasisType::Traits::RangeFieldType;

            using FiniteElementJacobianType =
                typename FiniteElement::Traits::LocalBasisType::Traits::JacobianType;

            Node &node_;
            TreePath &treePath_;
            std::size_t &j_ = 0;
            LocalInterpolateVisitor *visitor_ptr_;

          public:
            DerivativeWithNTRE(CallableWithNTRE<Node, TreePath> const &t)
                : node_(t.node_), treePath_(t.treePath_), j_(t.j_), visitor_ptr_(t.visitor_ptr_)
            {
            }

            FiniteElementJacobianType operator()(const LocalDomain &x) const
            {
              auto &&localDF_ = derivative(visitor_ptr_->localF_);
              const LocalFunctionDerivativeRange &y = localDF_(x);
              FiniteElementJacobianType
                  ret; // Assume this is of type FieldMatrix<1,d> TODO be more general here
              // I am not sure if this is correctly backwards compatible to the fufem-powerbases,
              // etc. But as far as I know, there are no fufem-classes using derivative
              // interpolation. This code should work if j_ == 0.
              // also assuming y has size dim (worldDimension)... necessary because y.size() returns
              // 1 for FieldMatrix<F,1,dim>
              //TODO Make this viable for Jacobians of other form ..
              for (std::size_t i = 0; i < dim; ++i)
                ret[0][i] = flatVectorView(
                    visitor_ptr_->nodeToRangeEntry_(node_, treePath_, y))[j_ * dim + i];
              return ret;
            }

            HessianWithNTRE<Node, TreePath> friend derivative(DerivativeWithNTRE<Node, TreePath> t)
            {
              return HessianWithNTRE<Node, TreePath>(t);
            }
          };

          template <typename Node, typename TreePath>
          class HessianWithNTRE
          {
          public:
            using FiniteElement = typename Node::FiniteElement;
            using FiniteElementRange =
                typename FiniteElement::Traits::LocalBasisType::Traits::RangeType;
            using FiniteElementRangeField =
                typename FiniteElement::Traits::LocalBasisType::Traits::RangeFieldType;

            using FiniteElementJacobianType =
                typename FiniteElement::Traits::LocalBasisType::Traits::JacobianType;
            // TODO be more general here
            using FiniteElementHessianType = FieldMatrix<FiniteElementRangeField, dim, dim>;
            Node &node_;
            TreePath &treePath_;
            std::size_t &j_ = 0;
            LocalInterpolateVisitor *visitor_ptr_;

          public:
            HessianWithNTRE(DerivativeWithNTRE<Node, TreePath> const &t)
                : node_(t.node_), treePath_(t.treePath_), j_(t.j_), visitor_ptr_(t.visitor_ptr_)
            {
            }

            FiniteElementHessianType operator()(const LocalDomain &x) const
            {
              auto &&localHF_ = derivative(derivative(visitor_ptr_->localF_));
              localHF_.bind(
                  visitor_ptr_->localF_.localContext()); // TODO remove, since !355 is merged

              // TODO deduce range of localDF_ with LocalDerivativeTraits
              const LocalFunctionHessianRange &y = localHF_(x);
              FiniteElementHessianType
                  ret; // Assume this is of type FieldMatrix<d,d> TODO be more general here
              // see comment in Derivativeclass
              for (std::size_t i = 0; i < dim; ++i)
                for (std::size_t j = 0; j < dim; ++j)
                  ret[i][j] = flatVectorView(visitor_ptr_->nodeToRangeEntry_(
                      node_, treePath_, y))[j_ * dim * dim + i * dim + j];
              return ret;
            }
          };

          LocalInterpolateVisitor(const B &basis, HV &coeff, const HBV &bitVector, const LF &localF,
                                  const LocalView &localView,
                                  const NodeToRangeEntry &nodeToRangeEntry)
              : localF_(localF), nodeToRangeEntry_(nodeToRangeEntry), vector_(coeff),
                bitVector_(bitVector), localView_(localView)
          {
            static_assert(Dune::Functions::Concept::isCallable<LocalFunction, LocalDomain>(),
                          "Function passed to LocalInterpolateVisitor does not model the "
                          "Callable<LocalCoordinate> concept");
          }

          template <typename Node, typename TreePath>
          void pre(Node &node, TreePath treePath)
          {
          }

          template <typename Node, typename TreePath>
          void post(Node &node, TreePath treePath)
          {
          }

          template <typename Node, typename TreePath>
          void leaf(Node &node, TreePath treePath)
          {
            using FiniteElement = typename Node::FiniteElement;
            using FiniteElementRange =
                typename FiniteElement::Traits::LocalBasisType::Traits::RangeType;
            using FiniteElementRangeField =
                typename FiniteElement::Traits::LocalBasisType::Traits::RangeFieldType;

            auto interpolationCoefficients = std::vector<FiniteElementRangeField>();
            auto &&fe = node.finiteElement();

            if constexpr (FiniteElement::Traits::LocalBasisType::Traits::dimRange == 1)
            {
              std::size_t j = 0;

              auto localFj = CallableWithNTRE(node, treePath, j, this);

              // We loop over j defined above and thus over the components of the
              // range type of localF_.

              auto blockSize = flatVectorView(vector_[localView_.index(0)]).size();

              for (j = 0; j < blockSize; ++j)
              {
                fe.localInterpolation().interpolate(localFj, interpolationCoefficients);

                for (size_t i = 0; i < fe.localBasis().size(); ++i)
                {
                  auto multiIndex = localView_.index(node.localIndex(i));
                  auto bitVectorBlock = flatVectorView(bitVector_[multiIndex]);
                  if (bitVectorBlock[j])
                  {
                    auto vectorBlock = flatVectorView(vector_[multiIndex]);
                    vectorBlock[j] = interpolationCoefficients[i];
                  }
                }
              }
            }
            else // ( FiniteElement::Traits::LocalBasisType::Traits::dimRange != 1 )
            {
              // TODO adapt this too, such that someone could implement hermite type FEs with
              // dimRange != 1 for all other finite elements: use the FiniteElementRange directly
              // for the interpolation
              auto localF = [&](const LocalDomain &x)
              {
                const auto &y = localF_(x);
                return FiniteElementRange(nodeToRangeEntry_(node, treePath, y));
              };

              fe.localInterpolation().interpolate(localF, interpolationCoefficients);
              for (size_t i = 0; i < fe.localBasis().size(); ++i)
              {
                auto multiIndex = localView_.index(node.localIndex(i));
                if (bitVector_[multiIndex])
                {
                  vector_[multiIndex] = interpolationCoefficients[i];
                }
              }
            }
          }
          // these members were protected, but CallableWithNTRE needs access
          // TODO improve this and make them protected again
          const LocalFunction &localF_;
          const NodeToRangeEntry &nodeToRangeEntry_;

        protected:
          VectorBackend &vector_;
          const BitVectorBackend &bitVector_;
          const LocalView &localView_;
        };

      } // namespace Imp

      /**
       * \brief Interpolate given function in discrete function space
       *
       * Only vector coefficients marked as 'true' in the
       * bitVector argument are interpolated.  Use this, e.g., to
       * interpolate Dirichlet boundary values.
       *
       * Notice that this will only work if the range type of f and
       * the block type of coeff are compatible and supported by
       * flatVectorView.
       *
       * \param basis Global function space basis of discrete function space
       * \param coeff Coefficient vector to represent the interpolation
       * \param f Function to interpolate
       * \param bitVector A vector with flags marking all DOFs that should be interpolated
       * \param nodeToRangeEntry Polymorphic functor mapping local ansatz nodes to range-indices of
       * given function
       */

      // TODO pass down derivative traits parameter
      template <class B, class C, class F, class BV, class NTRE>
      void interpolate(const B &basis, C &&coeff, const F &f, const BV &bv,
                       const NTRE &nodeToRangeEntry)
      {
        using GridView = typename B::GridView;
        using Element = typename GridView::template Codim<0>::Entity;

        using Tree = typename B::LocalView::Tree;

        using GlobalDomain = typename Element::Geometry::GlobalCoordinate;

        static_assert(
            Dune::Functions::Concept::isCallable<F, GlobalDomain>(),
            "Function passed to interpolate does not model the Callable<GlobalCoordinate> concept");

        auto &&gridView = basis.gridView();

        // Small helper functions to wrap vectors using istlVectorBackend
        // if they do not already satisfy the VectorBackend interface.
        auto toVectorBackend = [&](auto &v) -> decltype(auto)
        {
          if constexpr (models<Concept::VectorBackend<B>, decltype(v)>())
          {
            return v;
          }
          else
          {
            return istlVectorBackend(v);
          }
        };

        auto toConstVectorBackend = [&](auto &v) -> decltype(auto)
        {
          if constexpr (models<Concept::ConstVectorBackend<B>, decltype(v)>())
          {
            return v;
          }
          else
          {
            return istlVectorBackend(v);
          }
        };

        auto &&bitVector = toConstVectorBackend(bv);
        auto &&vector = toVectorBackend(coeff);
        vector.resize(sizeInfo(basis));

        // Make a grid function supporting local evaluation out of f
        auto gf = makeGridViewFunction(f, gridView);

        // Obtain a local view of f
        auto localF = localFunction(gf);

        auto localView = basis.localView();

        for (const auto &e : elements(gridView))
        {
          localView.bind(e);
          localF.bind(e);

          Imp::LocalInterpolateVisitor<B, Tree, NTRE, decltype(vector), decltype(localF),
                                       decltype(bitVector)>
              localInterpolateVisitor(basis, vector, bitVector, localF, localView,
                                      nodeToRangeEntry);
          TypeTree::applyToTree(localView.tree(), localInterpolateVisitor);
        }
      }

      /**
       * \brief Interpolate given function in discrete function space
       *
       * Only vector coefficients marked as 'true' in the
       * bitVector argument are interpolated.  Use this, e.g., to
       * interpolate Dirichlet boundary values.
       *
       * Notice that this will only work if the range type of f and
       * the block type of coeff are compatible and supported by
       * flatVectorView.
       *
       * \param basis Global function space basis of discrete function space
       * \param coeff Coefficient vector to represent the interpolation
       * \param f Function to interpolate
       * \param bitVector A vector with flags marking all DOFs that should be interpolated
       */
      template <class B, class C, class F, class BV>
      void interpolate(const B &basis, C &&coeff, const F &f, const BV &bitVector)
      {
        c1elements::interpolate(basis, coeff, f, bitVector, HierarchicNodeToRangeMap());
      }

      /**
       * \brief Interpolate given function in discrete function space
       *
       * Notice that this will only work if the range type of f and
       * the block type of coeff are compatible and supported by
       * flatVectorView.
       *
       * This function will only work, if the local ansatz tree of
       * the basis is trivial, i.e., a single leaf node.
       *
       * \param basis Global function space basis of discrete function space
       * \param coeff Coefficient vector to represent the interpolation
       * \param f Function to interpolate
       */
      template <class B, class C, class F>
      void interpolate(const B &basis, C &&coeff, const F &f)
      {
        c1elements::interpolate(basis, coeff, f, Imp::AllTrueBitSetVector(),
                                HierarchicNodeToRangeMap());
      }
    } // namespace c1elements
  }   // namespace Functions
} // namespace Dune

#endif // DUNE_FUNCTIONS_FUNCTIONSPACEBASES_C1_INTERPOLATE_HH
