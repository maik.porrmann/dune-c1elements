#ifndef DUNE_C1ELEMENTS_C1ELEMENTS_HH
#define DUNE_C1ELEMENTS_C1ELEMENTS_HH

#include <dune/functions/functionspacebases/hermitebasis.hh>
#include <dune/functions/functionspacebases/morleybasis.hh>
#include <dune/functions/functionspacebases/argyrisbasis.hh>
#include <dune/functions/functionspacebases/arnoldwintherbasis.hh>
// Note that we do not include interpolate.hh as we expect the discretization module to implement such a routine
#endif