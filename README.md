This module contains classes for the Hermite (1d-3d),
the Morley (2d), the Argyris(2d), and the Arnold-Winther element on simplices.
It covers both dune-localfunctions and dune-functions
aspects and thus mimics their directory structure. Additionally, the compatibility with the AMDiS module is provided in the `amdis/` directory.
All of those finite elements are not affine (Piola) equivalent and thus require an additional transformation when mapped onto the reference element. This approach follows the theory developed by Kirby for the fenicsx/firedrake framework.
For this theory, finite elements are grouped by the behavior of their degrees of freedom when pulled back onto the reference element. In short, assuming the polynomial space is preserved under pullback, the physical dofs are either
  1. mapped onto the reference dofs. This gives affine (Piola) equivalence and no transformation is necessary.
  2. mapped onto dofs whose span equals the span of the reference dofs (as infinite dimensional functionals). Then we have no affine (Piola) equivalence and a linear transformation of the basis functions is necessary. This Transformation is just the transposed of the basis transformation of the dofs. However, we have affine (Piola) interpolation equivalence, which means that we can implement the local interpolation via the inverse of the mentioned basis transformation.
  3. mapped onto a set of dofs, whose span does not equal to the span of reference dofs. So long as we have unisolvence however, the span on the restrictions of the dofs to the polynomial space is equal to the span of the restrictions of the reference dofs to the reference polynomial space and we can still derive a linear transformation like before. Again, the transformation of the basis functions is just the transposed the transformation of the dof restrictions. In this case however, we have no interpolation equivalence and cannot use the reference dofs to implement a local interpolation that can be used to determine coefficients of the physical finite element.

This behavior is modeled by a class structure `LinearTransformedLocalFiniteElement` similar to the `GlobalValuedLocalFiniteElement` structure, with corresponding classes `LinearTransformedLocalBasis`, etc. It is parametrized with a class that fulfills the `LinearTransformator` interface, which is essentially a objectified transformation class. This interface is defined as follows:
  - Like other objects in the dune-functions world, `LinearTransformator` objects need to be bound to an element to be useful.
  - The actual transformation is done by a call to the `apply` method. In the current implementation the interface has only one `apply` method, which should be used for shapevalues, gradients and hessians, since a basis transformation is essentially the same for all of those types.
  - Since different aspects like orientation and determination of boundary condition dofs require nonlocal information, the structure includes a `ElementInformation` interface, to be implemented by a class exported by the `LinearTransformator`.
  - Lastly, the interpolation is handled differently, depending on the type of finite element as described above.
    For finite elements of type (2), i.e. interpolation equivalent finite elements, the `LinearTransformator` class can implement an `applyInverse` method. The framework then provides a corresponding `LocalInterpolation` class.
    For finite elements of type (3), the `LinearTransformator` class needs to export a class `GlobalValuedInterpolation`, which can be bound to the `Element` and `ElementInformation` objects and additonally fulfills the `LocalIntepolation` interface.

Extensions of the `LocalFiniteElement` interface for Hermite/Morley/Argyris elements:
- The first three finite elements include derivative degrees of freedom,
and so their `LocalIntepolation` classes only work with
functions which provide a free `derivative` method. A
proposal implementation of dune-functions' `interpolate`
method is contained in the module.

- Strongly enforcing boundary conditions requires
setting a subset of boundary dofs. Typical routines like the ones in the examples don't
consider that. The `LocalCoefficient` classes for the three finite elements thus
offer an additional method to question whether a dof is in the
respective subset for Dirichlet or Clamped conditions.
Discretization modules that want to include this functionality thus
have to include them in their corresponding routines.


Preparing the Sources
=========================

Additional to the software mentioned in README you'll need the
following programs installed on your system:

  cmake >= 3.13

Getting started
---------------

If these preliminaries are met, you should run

  dunecontrol all

which will find all installed dune modules as well as all dune modules
(not installed) which sources reside in a subdirectory of the current
directory. Note that if dune is not installed properly you will either
have to add the directory where the dunecontrol script resides (probably
./dune-common/bin) to your path or specify the relative path of the script.

Most probably you'll have to provide additional information to dunecontrol
(e. g. compilers, configure options) and/or make options.

The most convenient way is to use options files in this case. The files
define four variables:

CMAKE_FLAGS      flags passed to cmake (during configure)

An example options file might look like this:

#use this options to configure and make if no other options are given
CMAKE_FLAGS=" \
-DCMAKE_CXX_COMPILER=g++-5 \
-DCMAKE_CXX_FLAGS='-Wall -pedantic' \
-DCMAKE_INSTALL_PREFIX=/install/path" #Force g++-5 and set compiler flags

If you save this information into example.opts you can pass the opts file to
dunecontrol via the --opts option, e. g.

  dunecontrol --opts=example.opts all

More info
---------

See

     dunecontrol --help

for further options.


The full build system is described in the dune-common/doc/buildsystem (Git version) or under share/doc/dune-common/buildsystem if you installed DUNE!
