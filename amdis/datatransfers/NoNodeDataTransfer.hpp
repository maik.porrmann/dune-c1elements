#pragma once
#include "dune/functions/functionspacebases/arnoldwintherbasis.hh"
#include <amdis/C1Elements.hpp>
#include <amdis/datatransfers/InterpolationDataTransfer.hpp>

namespace AMDiS {

/** Element-local data transfer on a single leaf node of the basis tree
 *  Handles computations related to the finite element basis node, in this case by doing nothing!
 * To be used for non hierarchic FE spaces by specializing the NodeDataTransfer class and inheriting
 * from this class.
 */
template <class Node, class Container, class Basis>
class NoNodeDataTransfer {
    using T = typename Container::value_type;
    // TODO cleanup commented lines
    using LocalView = typename Basis::LocalView;
    using Element = typename Node::Element;

    // using LocalBasis = typename Node::FiniteElement::Traits::LocalBasisType;
    // using LBRangeType = typename LocalBasis::Traits::RangeType;
    // using LocalInterpolation = typename Node::FiniteElement::Traits::LocalBasisType;
    // using LIDomainType = typename LocalInterpolation::Traits::DomainType;
    // using LIRangeType = typename LocalInterpolation::Traits::RangeType;

  public:
    using NodeElementData = std::vector<T>;

  public:
    NoNodeDataTransfer() = default;

    /// To be called once before cacheLocal/restrictLocal are called within the preAdapt step
    void preAdaptInit(LocalView const &lv, Container const &coeff, Node const &node) {
      // lv_ = &lv;
      // node_ = &node;
      // fatherNode_ = std::make_shared<Node>(node);
      // constCoeff_ = &coeff;
    }

    /// \brief Cache data on the element bound to node_
    /**
     * This functions is used whenever the element does not vanish and thus the
     * data can trivially be transferred to the new element
     **/
    // [[expects: preAdaptInit to be called before]]
    void cacheLocal(NodeElementData &dofs) const {
      // constCoeff_->gather(*lv_, *node_, dofs);
    }

    /** \brief Evaluate data on the child element bound to node_ and interpolate onto
     *  father entity using the coordinate transformation trafo from father to child.
     *
     * Stores cached data in the NodeElementData argument. After grid adaption the
     * data is copied by \ref copyLocal or \ref prolongLocal to the target element
     * in the new grid.
     *
     * \param father      The father element to interpolate to
     * \param fatherDOFs  Container to store the interpolated DOFs
     * \param trafo       Coordinate transform from local coordinates in father to local
     *                    coordinates in child element
     * \param childDOFs   DOF values from the child element
     * \param init        The father element is visited for the first time
     **/
    // [[expects: preAdaptInit to be called before]]
    template <class Trafo>
    bool restrictLocal(Element const &father, NodeElementData &fatherDOFs, Trafo const &trafo,
                       NodeElementData const &childDOFs, bool init) {
      return true;
    }

    /// To be called once before copyLocal/prolongLocal are called within the adapt step
    void adaptInit(LocalView const &lv, Container &coeff, Node const &node) {
      // lv_ = &lv;
      // node_ = &node;
      // fatherNode_ = std::make_shared<Node>(node);
      // coeff_ = &coeff;
    }

    /// \brief Copy already existing data to element bound to node_
    // [[expects: adaptInit to be called before]]
    void copyLocal(NodeElementData const &dofs) const {
      // coeff_->scatter(*lv_, *node_, dofs, Assigner::assign{});
    }

    /** \brief Interpolate data from father onto the child element bound to node_ using
     *  the transformation trafo from child to father
     *
     *  Stores the interpolated data from father to child in the container \ref coeff_.
     *
     * \param father      The father element
     * \param fatherDOFs  DOF values cached on the father element before adapt
     * \param trafo       Coordinate transform from local coordinates in child to local
     *                    coordinates in father element
     * \param init        Father element is visited for the first time
     **/
    // [[expects: adaptInit to be called before]]
    template <class Trafo>
    void prolongLocal(Element const &father, NodeElementData const &fatherDOFs, Trafo const &trafo,
                      bool init) {}

  private:
    LocalView const *lv_ = nullptr;
    Node const *node_ = nullptr;
    std::shared_ptr<Node> fatherNode_;
    Container const *constCoeff_ = nullptr;
    Container *coeff_ = nullptr;
    std::vector<bool> finishedDOFs_;
    NodeElementData fatherDOFsTemp_;
};

// Specialize NodeDataTransfer for c1 bases
template <class Container, class Basis, class GV, class R>
class NodeDataTransfer<Dune::Functions::ArgyrisNode<GV, R>, Container, Basis>
    : public NoNodeDataTransfer<Dune::Functions::ArgyrisNode<GV, R>, Container, Basis> {};

template <class Container, class Basis, class GV, class R>
class NodeDataTransfer<Dune::Functions::MorleyNode<GV, R>, Container, Basis>
    : public NoNodeDataTransfer<Dune::Functions::MorleyNode<GV, R>, Container, Basis> {};

template <class Container, class Basis, class GV, class R, bool b>
class NodeDataTransfer<Dune::Functions::HermiteNode<GV, R, b>, Container, Basis>
    : public NoNodeDataTransfer<Dune::Functions::HermiteNode<GV, R, b>, Container, Basis> {};

template <class Container, class Basis, class GV, class R>
class NodeDataTransfer<Dune::Functions::ArnoldWintherNode<GV, R>, Container, Basis>
    : public NoNodeDataTransfer<Dune::Functions::ArnoldWintherNode<GV, R>, Container, Basis> {};
} // namespace AMDiS