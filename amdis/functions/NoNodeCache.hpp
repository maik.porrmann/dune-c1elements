#pragma once
#include <amdis/functions/NodeCache.hpp>
#include <amdis/C1Elements.hpp>
#include <amdis/functions/Hessian.hpp>

namespace AMDiS{

/// \brief LeafNodeCache implementation that does not cache anything
  /**
   * \tparam Node  Type of the leaf basis node
   **/
  template <class Node>
  class NoLeafNodeCache: public Dune::TypeTree::LeafNode, public NodeWrapper<Node>
  {
  public:
    using BasisNode = Node;

    using FiniteElement = typename Node::FiniteElement;
    using LocalBasis = typename FiniteElement::Traits::LocalBasisType;

    using ShapeValues = std::vector<typename LocalBasis::Traits::RangeType>;
    using ShapeGradients = std::vector<typename LocalBasis::Traits::JacobianType>;
    using ShapeHessians = std::vector<Impl::Hessian_t<LocalBasis>>;

  private:
    using DomainType = typename LocalBasis::Traits::DomainType;
    static constexpr unsigned int dim = LocalBasis::Traits::dimDomain;

  protected:
    // Constructor storing a reference to the passed basis-node
    NoLeafNodeCache(Node const &basisNode): NodeWrapper<Node>(basisNode) {}

  public:
    /// Construct a new local-basis cache
    static NoLeafNodeCache create(Node const &basisNode) { return {basisNode}; }

    /// Return the local finite-element of the stored basis-node.
    FiniteElement const &finiteElement() const { return this->node_->finiteElement(); }

    /// Evaluate local basis functions at local coordinates
    ShapeValues localBasisValuesAt(DomainType const &local) const
    {
      // This might do a copy of the vector! But we trust the compiler to do copy elision
      ShapeValues data;
      this->localBasis().evaluateFunction(local, data);
      return data;
    }

    /// Evaluate local basis partial derivative at local coordinates
    // TODO Does this even make sense? You need all partials to transform them to global derivatives
    ShapeValues localPartialDerivativeAt(const std::array<unsigned int, dim> &order, DomainType const &local) const
    {
      ShapeValues data;
      this->localBasis().partial(order, local, data);
      return data;
    }

    /// Evaluate local basis jacobians at local coordinates
    ShapeGradients localBasisJacobiansAt(DomainType const &local) const
    {
      ShapeGradients data;
      this->localBasis().evaluateJacobian(local, data);
      return data;
    }

    // Evaluate local basis hessians at local coordinates
    ShapeHessians localBasisHessiansAt(DomainType const &local) const
    {
      ShapeHessians data;
      Impl::getHessians(this->localBasis(), data, local);
      return data;
    }

  private:
    /// Return the local-basis of the stored basis-node.
    LocalBasis const &localBasis() const { return this->node_->finiteElement().localBasis(); }
  };


  template <typename GV, typename R, bool b>
  class LeafNodeCache<Dune::Functions::HermiteNode<GV, R, b>>:
    public NoLeafNodeCache<Dune::Functions::HermiteNode<GV, R, b>>
  {
    using Node = Dune::Functions::HermiteNode<GV, R, b>;
    using Base = NoLeafNodeCache<Node>;

  public:
    LeafNodeCache<Node>(Node const &node): Base(node) {}
    /// Construct a new local-basis cache
    static LeafNodeCache<Node> create(Node const &basisNode) { return {basisNode}; }
  };

  template <typename GV, typename R>
  class LeafNodeCache<Dune::Functions::ArgyrisNode<GV, R>>:
    public NoLeafNodeCache<Dune::Functions::ArgyrisNode<GV, R>>
  {

    using Node = Dune::Functions::ArgyrisNode<GV, R>;
    using Base = NoLeafNodeCache<Node>;
    // using This = LeafNodeCache<Node>;

  public:
    LeafNodeCache<Node>(Node const &node): Base(node) {}
    /// Construct a new local-basis cache
    static LeafNodeCache<Node> create(Node const &basisNode) { return {basisNode}; }
  };

  template <typename GV, typename R>
  class LeafNodeCache<Dune::Functions::MorleyNode<GV, R>>:
    public NoLeafNodeCache<Dune::Functions::MorleyNode<GV, R>>
  {
    using Node = Dune::Functions::MorleyNode<GV, R>;
    using Base = NoLeafNodeCache<Node>;

  public:
    LeafNodeCache<Node>(Node const &node): Base(node) {}
    /// Construct a new local-basis cache
    static LeafNodeCache<Node> create(Node const &basisNode) { return {basisNode}; }
  };

  template <typename GV, typename R>
  class LeafNodeCache<Dune::Functions::ArnoldWintherNode<GV, R>>
      : public NoLeafNodeCache<Dune::Functions::ArnoldWintherNode<GV, R>> {
    using Node = Dune::Functions::ArnoldWintherNode<GV, R>;
    using Base = NoLeafNodeCache<Node>;

  public:
    LeafNodeCache<Node>(Node const &node) : Base(node) {}
    /// Construct a new local-basis cache
    static LeafNodeCache<Node> create(Node const &basisNode) { return {basisNode}; }
  };

} // namespace AMDiS

