from sympy import *
from sympy.polys.polyfuncs import horner
from sympy.abc import x, y
import symfem
import numpy as np
from symfem.elements.lagrange import Lagrange
from symfem.elements.aw import ArnoldWinther
from symfem.functions import parse_function_input as parse, _to_sympy_format
from ArnoldWinther import ArnoldWinther_Moment

def adaptReferenceElementToDune(fe):
  newRef = fe.reference
  assert(newRef.name == "triangle")
  newRef.edges = ((0,1),(0,2),(1,2))
  newFe = type(fe)(newRef, fe.order)
  return newFe

def adaptReferenceToPhysicalElement(fe, vertices):
  physRef = symfem.create_reference("triangle", vertices= vertices)
  assert(physRef.name == "triangle")
  # physRef.edges = ((0,1),(0,2),(1,2))
  newFe = type(fe)(physRef, fe.order)
  return newFe

def createGenericReferenceElement(refName, feName, order):
  return(adaptReferenceElementToDune(symfem.create_element(refName, feName, order)))

def createPhysicalElement(refName, feName, order, vertices):
  return(adaptReferenceToPhysicalElement(symfem.create_element(refName, feName, order), vertices))

def hornerScheme(f, derivative = [x,y], **kwargs):
  s = shape(f)
  assert(len(s) == 2)

  result =  [] #
  for i in range(s[0]):
    result.append([])
    for j in range(s[1]):
      if derivative is None: #values
        result[i].append(horner(f[i,j]))
      elif isinstance(derivative, list):  #multiple derivatives, typically jacobian
        result[i].append([])
        for k,direction in enumerate(derivative):
          result[i][j].append(horner(diff(f[i,j], direction), **kwargs))
      else:   # single derivative, i.e. partial
        result[i].append(horner(diff(f[i,j], derivative)))

  return result

def getCodeForList(f, **kwargs):
  code = ""
  if isinstance(f, list):
    code+= "{" + cxxcode(f[0], **kwargs)
    for ff in f[1:]:
      code += ", " + getCodeForList(ff, **kwargs)
    code += "}"
  else:
    code += cxxcode(f, **kwargs)
  return code

def getCodeForMatrix(tensor, **kwargs):
    symmetric = kwargs.pop("symmetric", False)
    code = ""
    if symmetric:
      code = "diag = "+ getCodeForList( tensor[0][1], **kwargs)+";\n"

    code += "\n*(iter++) = {{" + getCodeForList(tensor[0][0] , **kwargs)
    if symmetric:
      code += ",\t diag },\n\t{ diag ,\t"
    else :
      code += ",\t" + getCodeForList( tensor[0][1], **kwargs)+ "},\n\t{"  + getCodeForList( tensor[1][0], **kwargs)+  ",\t"
    code += getCodeForList( tensor[1][1], **kwargs)+"}};\n"
    return code

def getCodeForEvaluation(basis, **kwargs):
  symmetric = kwargs.get("symmetric")
  derivative = kwargs.pop("derivative", None)
  powsubs={'Pow': [(lambda b,e: e == 2, lambda b, e: ("{0}*{0}".format(b))),
  (lambda b,e: e == 3, lambda b, e: ("{0}*{0}*{0}".format(b))),
  (lambda b,e: e == 4, lambda b, e: ("{0}*{0}*{0}*{0}".format(b))),
  (lambda b, e: not b.is_integer, 'pow')]}
  code = "// generated with sympy from symfem library\n"
  code += "auto const&x = in[0], y = in[1];"

  if (symmetric):
    if derivative is None or not isinstance(derivative, list):
      code += "\ndouble diag;"
    elif isinstance(derivative[0], list):
      code += "\n FieldMatrix<double, " + str(len(derivative)) + ", " + str(len(derivative[0])) + ">diag ;"
    else:
      code += "\n FieldVector<double, " + str(len(derivative)) + "> diag;"

  kwargs.update( {"user_functions": powsubs})
  name = "val" if kwargs.get("assign_to") is None else kwargs.get("assign_to").name
  # kwargs.pop("assign_to")
  for i,f in enumerate(basis):
    code += "\n//{}th basis function\n".format(i+1)

    mat = hornerScheme(f, derivative)
    code += getCodeForMatrix(mat,**kwargs)
  return code

def printEvaluationCode(name ,fe = None, feArgs = None, symmetric = False):

  assert(isinstance(name, str))
  if fe is None:
    if feArgs is None:
      # feArgs = ("triangle", "AW", 3)
      raise Exception("Invalid argument! Please provide either fe object or Arguments to forward to symfem")
    fe = createGenericReferenceElement(*feArgs)

  basis = fe.get_basis_functions()
  grad = [x,y] # TODO generalize this for dim 1 and 3


  code = "#ifndef DUNE_C1ELEMENTS_" + name.upper() + "_INC_HH\n#define DUNE_C1ELEMENTS_" + name.upper() + "_INC_HH\n namespace Dune{\n  namespace Impl{ \n    "
  code += '// generated with sympy from symfem library\ntemplate<class D, class R>\n      void ' + name + 'LocalBasis<D,R>::evaluateFunction(const typename Traits::DomainType &in,std::vector<typename Traits::RangeType> &out) const\n{\nout.resize(size());\n auto iter = out.begin();'
  code += getCodeForEvaluation(basis, symmetric = symmetric)
  code += "\n}"

  print(code)
  code_derivative = '// generated with sympy from symfem library\ntemplate<class D, class R>\n      void ' + name + 'LocalBasis<D,R>::evaluateJacobian(const typename Traits::DomainType &in,std::vector<typename Traits::JacobianType> &out) const\n{\nout.resize(size());\nauto iter = out.begin();'
  code_derivative += getCodeForEvaluation(basis, derivative = grad, symmetric = symmetric)

  code_derivative += "\n}"

  print(code_derivative)

  code_partial = "// generated with sympy from symfem library\ntemplate<class D, class R>\n      void " + name + "LocalBasis<D,R>::partial(const std::array<unsigned int, dim> &order, const typename Traits::DomainType &in, std::vector<typename Traits::RangeType> &out) const\n {\n       out.resize(size());\n       auto totalOrder = std::accumulate(order.begin(), order.end(), 0);\nif (totalOrder == 0) {\n  evaluateFunction(in, out);\n}\n else if (totalOrder == 1) {\n "
  for j,direction in enumerate(grad):
    code_partial += "if(order[{}]==1)\n  {{ \nauto iter = out.begin();\n".format(j)
    code_partial += getCodeForEvaluation(basis, derivative = direction, symmetric = symmetric) + "}\nelse "

  code_partial += "\nDUNE_THROW(NotImplemented, \"Invalid partial derivative\");"
  code_partial += "\n}else \n    DUNE_THROW(NotImplemented, \"Higher order derivatives are not implemented\"); }\n"

  print(code_partial)
  print( "  }//namespace Impl\n}//namespace Dune\n#endif" ) # namespace braces

if __name__ == "__main__":
  init_printing()
  ## this is the AW element as defined in symfem, i.e. it uses a Lagrange basis as "testfunctions" for the Moment-dofs
  feArgs = ("triangle", "AW", 3)
  fe = createGenericReferenceElement(*feArgs)

  ## This modified version uses the common moments of order 0 and order 1
  other = createGenericReferenceElement("triangle", "AW-Moment", 3)

  printEvaluationCode("ArnoldWinther", other, symmetric = True)
